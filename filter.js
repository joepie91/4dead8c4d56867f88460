listOfNumbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

newList = listOfNumbers.filter(function(number) {
	return (number <= 5);
});

console.log(newList); // [ 0, 1, 2, 3, 4, 5 ]
